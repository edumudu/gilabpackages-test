import Vue, { PluginFunction, VueConstructor } from 'vue';


interface InstallFunction extends PluginFunction<any> {
  installed?: boolean;
}

declare const GilabPackagesTest: { install: InstallFunction };
export default GilabPackagesTest;

export const GilabPackagesTestSample: VueConstructor<Vue>;
